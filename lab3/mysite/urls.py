from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^manager/$', views.file_manager, name="file_manager"),
    url(r'^$', views.trash_list, name='trash_list'),
    url(r'^trash/(?P<pk>[0-9]+)/$', views.trash_detail, name='trash_detail'),
    url(r'^trash/new/$', views.trash_new, name='trash_new'),
    url(r'^tasks/$', views.task_list, name='tasks_list'),
    url(r'^regex/$', views.regex, name='regex'),
    url(r'^trash/change/(?P<pk>[0-9]+)/$', views.trash_change, name='trash_change'),
]
