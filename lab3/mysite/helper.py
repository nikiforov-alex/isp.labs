import os
from smrm.checker import get_size_trash


class FileDesc(object):

    def __init__(self, path):
        self.path = path

    def get_name(self):
        return os.path.basename(self.path)

    def get_path(self):
        return self.path

    def is_dir(self):
        return os.path.isdir(self.path)

    def get_size(self):
        return get_size_trash(self.path)
