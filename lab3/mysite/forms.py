from django import forms
from .models import TrashList, TaskList


class TrashForm(forms.ModelForm):
    class Meta:
        model = TrashList
        fields = ('name', 'path_trash', 'size', 'policy_delete', 'policy_clean',
                  'policy_clean_date', 'policy_clean_size', 'dry_run')


class TaskForm(forms.ModelForm):
    class Meta:
        model = TaskList
        fields = ('task_status', 'action', 'trash_obj', 'files')
