# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
from .models import TrashList, TaskList
from .forms import TrashForm
from django.shortcuts import redirect
from mysite.helper import FileDesc
from smrm import trash as trash_1
import os
import shutil
import time


def file_manager(request):
    path = request.GET.get('path')
    if not path:
        path = '~'
    path = os.path.expanduser(path)
    file_list = []
    for name in os.listdir(path):
        file_list.append(FileDesc(os.path.join(path, name)))
    if request.method == "POST":
        trash_name = request.POST['trash_cur']
        trash = TrashList.objects.filter(pk=trash_name)[0]
        print(request.POST)
        list_files = request.POST.getlist('selected_files')
        if len(list_files) > 0:
            task = TaskList.objects.create(task_status='inprogress', action='delete', trash_obj=trash,
                                           files=list_files, regex='*')
            try:
                task.run()
                task.task_status = 'complete'
            except Exception as e:
                print e, '!!!'
                task.task_status = 'error'
            finally:
                task.save()

            return redirect('tasks_list')
        file_list.sort()
    return render(request, 'file_manager.html', {'previos_dir': os.path.abspath(os.path.join(path, os.pardir)),
                                                 'curent_dir': path,
                                                 'trashs': TrashList.objects.all(),
                                                 'files': file_list})

def trash_list(request):
    trashs = TrashList.objects.all()
    return render(request, 'trash_list.html', {'trashs': trashs})


def trash_new(request):
    if request.method == "POST":
        form = TrashForm(request.POST)
        if form.is_valid():
            trash = form.save(commit=False)
            if not os.path.exists(trash.path_trash):
                trash.path_trash = '/home/alexander/'
            # trash.author = request.user
            t = trash_1.Trash(dir_for_trash=trash.path_trash, name=trash.name)
            # t = trash.Trash(dir_for_trash=trash.path_trash, name=trash.name)
            trash.save()
            return redirect('trash_detail', pk=trash.pk)
    else:
        form = TrashForm()
    return render(request, 'trash_new.html', {'form': form})


def trash_change(request, pk):
    trash = get_object_or_404(TrashList, pk=pk)
    old_path = trash.path_trash
    old_name = trash.name
    if request.method == "POST":
        form = TrashForm(request.POST, instance=trash)
        if form.is_valid():
            trash = form.save(commit=False)
            if not os.path.exists(trash.path_trash) or not os.path.isdir(trash.path_trash):
                trash.path_trash = old_path
            print(old_path)
            print(trash.path_trash)
            if os.path.join(old_path, old_name) is not os.path.join(trash.path_trash, trash.name) and\
                    not os.path.exists(os.path.join(trash.path_trash, trash.name)):
                os.rename(os.path.join(old_path, old_name), os.path.join(trash.path_trash, trash.name))
            else:
                trash.name = old_name
                trash.path_trash = old_path
            # trash.author = request.user
            t = trash_1.Trash(dir_for_trash=trash.path_trash, name=trash.name)
            if trash.cur_size() > trash.size:
                trash.size = trash.cur_size()
            trash.save()
            return redirect('trash_detail', pk=trash.pk)
    else:
        form = TrashForm(instance=trash)
    return render(request, 'trash_change.html', {'form': form})


def trash_detail(request, pk):
    trash = get_object_or_404(TrashList, pk=pk)
    t = trash_1.Trash(dir_for_trash=trash.path_trash, name=trash.name)
    list_dir = []
    in_trash = os.path.join(os.path.join(trash.path_trash, trash.name), 'files')
    for name in os.listdir(in_trash):
        list_dir.append(FileDesc(os.path.join(in_trash, name)))
    if request.method == "POST":
        list_files = request.POST.getlist('selected_files')
        if 'change' in request.POST:
            return redirect('trash_change', pk=trash.pk)
        if 'delete' in request.POST:
            trash.delete()
            print(os.path.join(trash.path_trash, trash.name))
            shutil.rmtree(os.path.join(trash.path_trash, trash.name))
            return render(request, 'trash_list.html', {'trashs': TrashList.objects.all()})
        if 'removeall' in request.POST:
            if len(os.listdir(os.path.join(os.path.join(trash.path_trash, trash.name), 'files'))) > 0:
                task = TaskList.objects.create(task_status='inprogress', action='clear trash', trash_obj=trash,
                                               files=list_files, regex='*')
                try:
                    task.run()
                    print('complete')
                    task.task_status = 'complete'
                    task.save()
                except:
                    task.task_status = 'error'
                    task.save()
                return redirect('tasks_list')
        if 'restoreall' in request.POST:
            if len(os.listdir(os.path.join(os.path.join(trash.path_trash, trash.name), 'files'))) > 0:
                task = TaskList.objects.create(task_status='inprogress', action='restore all', trash_obj=trash,
                                               files=os.listdir(os.path.join(os.path.join(trash.path_trash, trash.name), 'files')),
                                               regex='*')
                try:
                    print('!!!!!!')
                    task.run()
                    print('complete')
                    task.task_status = 'complete'
                    task.save()
                except:
                    task.task_status = 'error'
                    task.save()
                return redirect('tasks_list')
        if 'remove' in request.POST:
            if len(list_files) > 0:
                task = TaskList.objects.create(task_status='inprogress', action='remove from trash', trash_obj=trash,
                                               files=list_files, regex='*')
                try:
                    print('!!!!!!')
                    task.run()
                    print('complete')
                    task.task_status = 'complete'
                    task.save()
                except Exception as e:
                    print(e)
                    task.task_status = 'error'
                    task.save()
                return redirect('tasks_list')
        if 'restore' in request.POST:
            if len(list_files) > 0:
                task = TaskList.objects.create(task_status='inprogress', action='restore', trash_obj=trash,
                                               files=list_files, regex='*')
                try:
                    task.run()
                    print('complete')
                    task.task_status = 'complete'
                    task.save()
                except:
                    task.task_status = 'error'
                    task.save()
                return redirect('tasks_list')
    return render(request, 'trash_detail.html', {'trash': trash, 'list_dir': list_dir})


def task_list(request):
    tasks = TaskList.objects.all()
    if request.POST:
        tasks = TaskList.objects.all()
        tasks.delete()
    return render(request, 'task_list.html', {'tasks': reversed(tasks)})


def regex(request):
    if request.method == "POST":
        regulex = request.POST.get('regular')
        path = request.POST.get('path_reg')
        trash_name = request.POST['trash_cur']
        trash = TrashList.objects.filter(pk=trash_name)[0]
        task = TaskList.objects.create(task_status='inprogress', action='delete regex', trash_obj=trash,
                                       files=path, regex=regulex)
        try:
            task.run()
            print('complete')
            task.task_status = 'complete'
            task.save()
        except:
            task.task_status = 'error'
            task.save()
        print(task.task_status)
        return redirect('tasks_list')
    return render(request, 'regex_delete.html', {'trashs': TrashList.objects.all()})
