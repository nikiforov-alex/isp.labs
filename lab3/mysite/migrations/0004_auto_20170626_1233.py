# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-26 12:33
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mysite', '0003_auto_20170624_0851'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrashList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('path_trash', models.CharField(max_length=256)),
                ('size', models.IntegerField()),
                ('policy_delete', models.CharField(default='collision', max_length=16)),
                ('policy_clean', models.CharField(default='size', max_length=8)),
                ('policy_clean_date', models.DateTimeField(verbose_name='date and time for cleaning trash')),
                ('policy_clean_size', models.CharField(max_length=10)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='trash_list',
            name='author',
        ),
        migrations.DeleteModel(
            name='Trash_List',
        ),
    ]
