# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-08-30 08:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0004_auto_20170626_1233'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task_status', models.CharField(choices=[('complete', 'complete'), ('inprogress', 'inprogress'), ('error', 'error')], max_length=20)),
                ('action', models.CharField(choices=[('delete', 'delete'), ('restore', 'restore')], max_length=50)),
                ('files', models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name='trashlist',
            name='policy_clean',
            field=models.CharField(choices=[('size', 'size'), ('date', 'date')], default='size', max_length=8),
        ),
        migrations.AlterField(
            model_name='trashlist',
            name='policy_clean_date',
            field=models.DateTimeField(default=b'2017-08-30 08:02:24', verbose_name='date and time for cleaning trash'),
        ),
        migrations.AlterField(
            model_name='trashlist',
            name='policy_delete',
            field=models.CharField(choices=[('collision', 'collision'), ('replace', 'replace')], default='collision', max_length=16),
        ),
        migrations.AddField(
            model_name='tasklist',
            name='trash_obj',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mysite.TrashList'),
        ),
    ]
