# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import datetime
import pathos.multiprocessing as multiprocessing
from django.db import models
from smrm import trash
from smrm import checker
import time


class TrashList(models.Model):
    policy_list = (
        ('collision', 'collision'),
        ('replace', 'replace'),
    )
    policy_clean_list = (
        ('size', 'size'),
        ('date', 'date'),
    )
    # author = models.ForeignKey('auth.User')
    name = models.CharField(max_length=200, default='trash', unique=True)
    path_trash = models.CharField(max_length=256, default='~')
    size = models.IntegerField(verbose_name='max size trash(kB)', default=1048576)
    policy_delete = models.CharField(max_length=16, default='collision', choices=policy_list)
    policy_clean = models.CharField(max_length=8, default='size', choices=policy_clean_list)
    policy_clean_date = models.DateTimeField(verbose_name='date and time for cleaning trash',
                                             default=str(datetime.datetime.now()).strip()[:19])
    policy_clean_size = models.IntegerField(verbose_name='count large object for cleaning trash', default=3)
    dry_run = models.BooleanField(verbose_name='activate dry-run', default=False)

    def cur_size(self):
        temp_trash = self.init_trash()
        return checker.get_size_trash(os.path.join(os.path.join(self.path_trash, self.name), 'files'))/1024

    def init_trash(self):
        return trash.Trash(dir_for_trash=self.path_trash, size_trash=self.size*1024, policy_delete=self.policy_delete,
                           policy_clean=self.policy_clean, policy_clean_date=self.policy_clean_date,
                           policy_clean_size=self.policy_clean_size, name=self.name, force=True, dry_run=self.dry_run)


    def deleter(self, files):
        temp_trash = self.init_trash()
        pool_size = multiprocessing.cpu_count() * 2
        pool = multiprocessing.Pool(processes=pool_size,
                                    maxtasksperchild=2,
                                    )
        time.sleep(1)
        pool.map(temp_trash.delete_file, files)
        pool.close()
        pool.join()

    def recoverer(self, files):
        list_file = []
        for f in files:
            list_file.append(os.path.split(f)[1])
        temp_trash = self.init_trash()
        pool_size = multiprocessing.cpu_count() * 2
        pool = multiprocessing.Pool(processes=pool_size,
                                    maxtasksperchild=2,
                                    )
        pool.map(temp_trash.recovery, list_file)
        pool.close()
        pool.join()
        # for file in files:
        #     temp_trash.recovery(os.path.split(file)[1])

    def regex_del(self, path, regex):
        temp_trash = self.init_trash()
        temp_trash.delete_by_re(path, str(regex))

    def cleaner(self):
        temp_trash = self.init_trash()
        temp_trash.delete_trash()

    def remove(self, files):
        temp_trash = self.init_trash()
        for file in files:
            file = os.path.split(file)[1]
            path_files = os.path.join(os.path.join(temp_trash.dir_for_trash, self.name), 'files')
            path_info = os.path.join(os.path.join(temp_trash.dir_for_trash, self.name), 'info')
            checker.remove_object(os.path.join(path_files, file))
            checker.remove_object(os.path.join(path_info, file))

    def __str__(self):
        return self.name


class TaskList(models.Model):
    ACTION = (
        ('delete', 'delete'),
        ('restore', 'restore'),
        ('delete regex', 'delete regex'),
        ('remove from trash', 'remove from trash'),
        ('clear trash', 'clear trash'),
        ('restore all', 'restore all'),
    )
    STATUS = (
        ('complete', 'complete'),
        ('inprogress', 'inprogress'),
        ('error', 'error'),
    )
    task_status = models.CharField(max_length=20, choices=STATUS)
    action = models.CharField(max_length=50, choices=ACTION)
    trash_obj = models.ForeignKey(TrashList)
    files = models.TextField()
    regex = models.CharField(max_length=20, default='+')

    def run(self):
        if self.action == 'delete':
            self.trash_obj.deleter(self.files)
        if self.action == 'restore':
            self.trash_obj.recoverer(self.files)
        if self.action == 'delete regex':
            self.trash_obj.regex_del(self.files, self.regex)
        if self.action == 'clear trash':
            self.trash_obj.cleaner()
        if self.action == 'remove from trash':
            self.trash_obj.remove(self.files)
        if self.action == 'restore all':
            self.trash_obj.recoverer(os.listdir(os.path.join(os.path.join(self.trash_obj.path_trash, self.trash_obj.name), 'files')))

    def __str__(self):
        return self.task_status