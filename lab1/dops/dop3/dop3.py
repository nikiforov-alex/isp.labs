import re
import pickle


if __name__ == "__main__":
    store = set()
    while True:
        instr = raw_input('key: ')
        if re.match('grep ', instr):
            str1 = ''
            for greps in store:
                if re.search(instr.split()[1], greps) != None:
                    print greps
        else:
            list_input = []
            for i in xrange(1, len(instr.split())):
                list_input.append((re.sub(',', '', instr).split()[i]))

            if re.match('add ', instr):
                for i in xrange(len(instr.split())-1):
                    store.add(list_input[i])
            elif re.match('remove ', instr):
                store.discard(list_input[0])
            elif re.match('find ', instr):
                print (store & set([list_input[0]])) != set()
            elif re.match('list', instr):
                print list(store)
            elif re.match('save', instr):
                with open('set_list', 'w') as file_p:
                    pickle.dump(store, file_p)
            elif re.match('load', instr):
                with open('set_list', 'r') as file_p:
                    store = pickle.load(file_p)
            elif re.match('exit', instr):
                break
            else:
                print "Incorrect command"


