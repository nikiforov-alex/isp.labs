import copy


def quicksort(mylist, start, end):
    if start < end:
        # partition the list
        pivot = partition(mylist, start, end)
        # sort both halves
        quicksort(mylist, start, pivot-1)
        quicksort(mylist, pivot+1, end)
    return mylist


def partition(mylist, start, end):
    pivot = mylist[start]
    left = start+1
    right = end
    done = False
    while not done:
        while left <= right and mylist[left] <= pivot:
            left += 1
        while mylist[right] >= pivot and right >= left:
            right -= 1
        if right < left:
            done = True
        else:
            # swap places
            mylist[left], mylist[right] = mylist[right], mylist[left]
    # swap start with myList[right]
    mylist[start], mylist[right] = mylist[right], mylist[start]
    return right


def merge_sort(alist):
    if len(alist)>1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        merge_sort(lefthalf)
        merge_sort(righthalf)

        i, j, k = 0, 0, 0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k] = lefthalf[i]
                i += 1
            else:
                alist[k]=righthalf[j]
                j += 1
            k += 1

        while i < len(lefthalf):
            alist[k] = lefthalf[i]
            i += 1
            k += 1

        while j < len(righthalf):
            alist[k] = righthalf[j]
            j += 1
            k += 1
    return alist


def rang_sort(A):
    length = len(str(max(A)))
    rang = 10
    for i in range(length):
        B = [[] for k in range(rang)]
        for x in A:
            figure = x // 10**i % 10
            B[figure].append(x)
        A = []
        for k in range(rang):
            A += B[k]
    print A


if __name__ == "__main__":
    L = [int(cn) for cn in open('numbers.txt', 'r').read().split()]
    L1 = copy.copy(L)
    quicksort(L, 0, len(L)-1)
    merge_sort(L1)
    for cn in L:
        print cn,
    print
    for cn in L1:
        print cn,
    print
    rang_sort(L)
