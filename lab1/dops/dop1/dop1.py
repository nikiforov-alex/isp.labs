import re
from collections import Counter


def count_dict_word(text_f, k=10, n=4):

    list_word = [val1 for val1 in re.split('[ ,.?!;\n]', text_f) if (val1 != '')]
    dict_word = Counter(list_word)
    sumk = 0
    for key in sorted(dict_word.items(), key=lambda (k, v): v, reverse=True):
        if (len(key[0]) == n) and (sumk < k):
            print key[0],
            sumk += 1
    print
    for key1 in dict_word:
        print key1, '=>', dict_word[key1]


def word_in_str(str_w):
    """
        Number word in sentence
    """
    list_word = [val1 for val1 in re.split('[ ,.?!\n]', str_w) if (val1 != '')]
    summ = 0
    for count in list_word:
        summ += 1
    return summ


def middle_median_value(text_f):
    list_str = re.split('[.!?]', text_f)
    list_count = list();
    midle_count = 0
    count_word = 0
    for str_word in list_str:
        list_count.append(word_in_str(str_word))
    for value in list_count:
        midle_count += value
        count_word += 1
    print "midle count: ", midle_count/count_word
    list_count.sort()
    print "Median count: ",
    if len(list_count) % 2 == 1:
        print list_count[len(list_count)/2]
    else:
        print float(list_count[len(list_count)//2-1]+list_count[len(list_count)//2])/2


if __name__ == "__main__":
    f = open('text1', 'r')
    text_f1 = f.read()
    k = input('input k:')
    n = input('input n:')
    middle_median_value(text_f1)
    count_dict_word(text_f1, k, n)










