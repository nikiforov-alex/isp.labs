import json
import os
import re
import datetime


def create_config(path_conf, **flags):
    config_diction = {}
    for i in flags:
        if not flags[i]:
            config_diction[i] = parse_config_default(path_conf, i)
        else:
            config_diction[i] = check_config(i, flags[i])
            print i, '= ', config_diction[i]
    with open(os.path.join(path_conf, 'config_file_json'), 'w') as conf1:
        conf1.write(json.dumps(config_diction, indent=0))
    with open(os.path.join(path_conf, 'config_file'), 'w') as conf:
        conf.writelines('path from trash is ' + '\n\t' + str(config_diction['path']) + '\n\n')
        conf.writelines('Date for cleaning trash: ' + '\n\t' + str(config_diction['date']) + '\n\n')
        conf.writelines('Amount of largest removable objects is ' + '\n\t' + str(config_diction['size']) + '\n\n')
        conf.writelines('Delete and restore policy:' + '\n\t' + str(config_diction['policy']) + '\n\n')
        conf.writelines('Policy cleaning trash:' + '\n\t' + str(config_diction['cleaning']) + '\n\n')
        conf.writelines('Max size trash:' + '\n\t' + str(config_diction['size_trash']) + '\n\n')


def check_config(flag, value):
    if not os.path.exists(str(value)) and flag is 'path':
        return os.path.expanduser('~')
    if not re.match(str(value), '[0-9]{4,4}-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]+') \
            and flag is 'date':
        return str(datetime.datetime.now())
    if not str(value).isdigit() and flag is 'size':
        return '2'
    if value not in ['replace', 'collision'] and flag is 'policy':
        return 'collision'
    if value not in ['size', 'date'] and flag is 'cleaning':
        return 'size'
    if not str(value).isdigit() and flag is 'size_trash':
        return '10000'
    return value


def parse_config_default(path_conf, flag):
    if flag is 'path':
        return parse_config(path_conf, path=True)
    elif flag is 'size':
        return parse_config(path_conf, size=True)
    elif flag is 'date':
        return parse_config(path_conf, date=True)
    elif flag is 'policy':
        return parse_config(path_conf, policy=True)
    elif flag is 'cleaning':
        return parse_config(path_conf, cleaning=True)
    elif flag is 'size_trash':
        return parse_config(path_conf, size_trash=True)
    else:
        raise IOError


def show_config(path_conf):
    with open(os.path.join(path_conf, 'config_file'), 'r') as f1:
        print f1.read()


def parse_config(path_conf, path=False, size=False, date=False, policy=False,
                 cleaning=False, size_trash=False, parse_format='JSON'):
    """
    parse configuration file
    """
    configs = []
    settings = ['path', 'date', 'size', 'policy', 'cleaning', 'size_trash']
    if parse_format is 'JSON':
        with open(os.path.join(path_conf, 'config_file_json'), 'r') as conf1:
            dict_conf = json.loads(str(conf1.read()))
        for k in settings:
            configs.append(dict_conf[k])
    else:
        for k in [1, 4, 7, 10, 13, 16]:
            with open(os.path.join(path_conf, 'config_file'), 'r') as f1:
                configs.append(str(f1.read().split('\n')[k].strip()))
    if path is True:
        return configs[0]
    if date is True:
        return configs[1]
    if size is True:
        return configs[2]
    if policy is True:
        return configs[3]
    if cleaning is True:
        return configs[4]
    if size_trash is True:
        return configs[5]
