import os
import checker as checker


def auto_cleaning(answer, trash):
    if answer in ['y', 'Y']:
        policy = trash.policy_clean
        if policy == 'size':
            auto_delete_size(trash)
        if policy == 'date':
            auto_delete_date(trash)
        return False
    else:
        return True


def auto_delete_date(trash):
    for i in os.listdir(trash.info_path):
        path_i = os.path.join(trash.info_path, i)
        with open(path_i, 'r') as f2:
            if f2.read().split('\n')[1] < trash.policy_clean_date:
                if not trash.dry_run:
                    checker.remove_object(path_i)
                    os.remove(os.path.join(trash.file_path, os.path.split(path_i)[1]))
    checker.silent_code('Successfully clear old objects in trash', trash)


def auto_delete_size(trash):
    dict_file = {}
    tuple_file = []
    count_max_file = trash.policy_clean_size
    count = 0
    for i in os.listdir(trash.file_path):
        dict_file[str(i)] = int(checker.get_size_trash(os.path.join(trash.file_path, i)))
        count = count_max_file
    for i in sorted(dict_file.items(), key=lambda (k, v): v, reverse=True):
        tuple_file.append(i)
    for d in xrange(int(count)):
        if not trash.dry_run:
            checker.remove_object(os.path.join(trash.file_path, tuple_file[d][0]))
            checker.remove_object(os.path.join(trash.info_path, tuple_file[d][0]))
        print 'delete: ', tuple_file[d][0]
    checker.silent_code('Successfully clear %s large objects in trash' % count_max_file, trash)
