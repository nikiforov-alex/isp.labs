import os
from unittest import TestCase

import smrm.config as config
from smrm.trash import Trash


class TestTrash(TestCase):

    test_trash = Trash()  # !!!

    def test_make_dir(self):
        test_path = Trash.trash_path
        self.test_trash.make_dir(test_path)
        self.assertTrue(os.path.exists(test_path))

    def test_delete_file(self):
        file1 = './file.txt'
        with open(file1, 'w') as f:
            pass
        self.test_trash.delete_file(file1)
        self.assertFalse(os.path.exists(file1))
        self.assertTrue(os.path.exists(os.path.join(self.test_trash.file_path, 'file.txt')))
        self.assertTrue(os.path.exists(os.path.join(self.test_trash.info_path, 'file.txt')))

    def test_restore_file(self):
        file1 = 'file.txt'
        self.test_trash.recovery(file1)
        self.assertFalse(os.path.exists(os.path.join(self.test_trash.file_path, 'file.txt')))
        self.assertFalse(os.path.exists(os.path.join(self.test_trash.info_path, 'file.txt')))
        self.assertTrue(os.path.exists(os.path.join('.', file1)))


    #def test_remove_from_trash(self):
    #    self.fail()

    def test_clean_trash(self):
        self.test_trash.delete_trash()
        self.assertEqual(0, len(os.listdir(self.test_trash.file_path)))
        self.assertEqual(0, len(os.listdir(self.test_trash.file_path)))

    def test_auto_clean_date(self):
        date = config.parse_config(date=True)
        check = True
        self.test_trash.auto_delete_date()
        for i in os.listdir(self.test_trash.info_path):
            with open(os.path.join(self.test_trash.info_path, i), 'r') as f:
                date_file = f.read().split('\n')[1]
                if date_file < date:
                    self.fail('error')
        self.assertEqual(check, True)

    def test_auto_clean_size(self):
        count = len(os.listdir(self.test_trash.file_path))
        count_del = config.parse_config(size=True)
        self.test_trash.auto_delete_size()
        cur_count = len(os.listdir(self.test_trash.file_path))
        self.assertEqual(str(count_del), str(count-cur_count))

    def test_remove_by_re(self):
        file1 = './abvgde.txt'
        file2 = './abvgde2.txt'
        with open(file1, 'w') as f:
            pass
        with open(file2, 'w') as f:
            pass
        self.test_trash.delete_by_re('.', 'abvgde')
        for i in [file1, file2]:
            self.assertFalse(os.path.exists(i))
            self.assertTrue(os.path.exists(os.path.join(self.test_trash.file_path, os.path.split(i)[1])))
            self.assertTrue(os.path.exists(os.path.join(self.test_trash.info_path, os.path.split(i)[1])))

