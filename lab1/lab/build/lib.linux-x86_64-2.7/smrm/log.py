import logging
import os

logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s',
                    level=logging.INFO, filename=u'/home/alexander/isp.labs/lab1/lab/mylog.log')


def decorator(function):
    def wrapper(*args, **kwargs):
        try:
            function(*args, **kwargs)
            logging.info("function __ %s __ successful with arguments %s ; %s", function.__name__, args[1:], kwargs)
        except Exception as e:
            logging.error("ERROR %s____ IN FUNC __%s__ with arguments %s ; %s by path %s",
                          e, function.__name__, args[1:], kwargs, os.getcwd())
            # checker.silent_code(e, 111, kwargs.get('silent'))
    return wrapper
