import json
import os
import re
import datetime


@decorator
def create_config(path_trash='', date='', count_size='', policy='', cleaning='', size_trash=''):
    print path_trash, ' | ', date, ' | ', count_size
    if not os.path.exists(str(path_trash)) and not path_trash:
        path_trash = '/home/alexander/isp.labs/lab1/lab/'
    if not re.match(str(date), '[0-9]{4,4}-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]+') and not date:
        date = str(datetime.datetime.now())
    if not str(count_size).isdigit() and not count_size:
        count_size = 2
    if policy not in ['replace', 'collision'] and not policy:
        policy = 'collision'
    if cleaning not in ['size', 'date'] and not cleaning:
        cleaning = 'size'
    if not size_trash.isdigit() and not size_trash:
        size_trash = 10000
    with open('/home/alexander/isp.labs/lab1/lab/config_file_json', 'w+r') as conf1:
        config_diction = json.load(conf1.read())

        conf1.write(json.dumps(config_diction))
    print config_diction
    with open('/home/alexander/isp.labs/lab1/lab/config_file', 'w') as conf:
        conf.writelines('path from trash is ' + '\n\t' + str(path_trash) + '\n\n')
        conf.writelines('Date for cleaning trash: ' + '\n\t' + str(date) + '\n\n')
        conf.writelines('Amount of largest removable objects is ' + '\n\t' + str(count_size) + '\n\n')
        conf.writelines('Delete and restore policy:' + '\n\t' + str(policy) + '\n\n')
        conf.writelines('Policy cleaning trash:' + '\n\t' + str(cleaning) + '\n\n')
        conf.writelines('Max size trash:' + '\n\t' + str(size_trash) + '\n\n')


