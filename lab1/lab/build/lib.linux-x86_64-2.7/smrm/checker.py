import os
import re
import shutil
from config import create_config


def in_trash(folder, trash):
    if (folder == trash.file_path) or (folder == trash.info_path):
        silent_code("object already in trash", trash)
        raise OSError("object already in trash")


def exist_file(path, trash):
    if not os.path.exists(path):
        silent_code('file does not exist', trash)
        raise OSError('file does not exist')


def path_trash(my_path, trash):
    if os.path.isdir(my_path) and re.match(my_path, trash.trash_path):
        silent_code('this directory contain trash', trash)
        raise OSError('this directory contain trash')


def check_config(path_conf):
    if (not os.path.exists(os.path.join(str(path_conf), 'config_file_json'))
            and not os.path.exists(os.path.join(str(path_conf), 'config_file'))):
        create_config(path_conf, path=True, date=True, size=True, policy=True, cleaning=True, size_trash=True)


def silent_code(text, trash):
    if trash.silent is not True:
        print text


def interactive_mode(text, trash):
    if trash.interactive:
        silent_code(text, trash)
        interactive = raw_input()
        if interactive not in ['Y', 'y']:
            return False
        else:
            return True
    else:
        return True


def check_size_object(size_obj, trash):
    if int(size_obj) > int(trash.size_trash):
        silent_code('size object larger than size trash', trash)
        raise MemoryError('size object larger than size trash')


def path_exist_checker(name, trash=None):
    new_path = os.path.join(trash.file_path, name)
    new_path_info = os.path.join(trash.info_path, name)
    counter = len(new_path)
    count_i = len(new_path_info)
    count = 0
    if trash.policy_delete.strip() == 'collision':
        while os.path.lexists(new_path):
            count += 1
            new_path = new_path[:counter] + '.' + str(count)
            new_path_info = new_path_info[:count_i] + '.' + str(count)
    elif trash.policy_delete.strip() == 'replace':
        pass
    else:
        raise IOError('incorrect policy')
    return new_path, new_path_info


def force_ascker(text_print, trash):
    if trash.force:
        return 'Y'
    else:
        silent_code(text_print, trash)
        return raw_input()


def access(path, trash):
    if not os.access(path, os.W_OK):
        raise OSError('Access is denied')


def ascker_remove(answer, path, trash):
    if answer in ['y', 'Y']:
        if not trash.dry_run:
            os.remove(path)
        silent_code("object removed", trash)
        raise OSError("object removed")
    else:
        silent_code('the object remained in place', trash)
        raise OSError('the object remained in place')


def get_size_trash(path_dir, arr_file=None, arr_dirs=None):
    """
    This method may return size current object or list path of files in subtree
    """
    total_size = 0
    list_file = []
    list_dirs = []
    if os.path.isfile(path_dir):
        return os.path.getsize(path_dir)
    for d, dirs, files in os.walk(path_dir):
        for f in files:
            path = os.path.join(d, f)
            total_size += os.path.getsize(path)
            list_file.append(path)
        for di in dirs:
            path_d = os.path.join(d, di)
            list_dirs.append(path_d)
    if arr_dirs is not None:
        return list_dirs
    if arr_file is not None:
        return list_file
    return total_size


def remove_object(path):
    if os.path.isfile(path):
        os.remove(path)
    else:
        shutil.rmtree(path)


def exist_restore_file(past_path, policy):
    count = 0
    cnt = len(past_path)
    if policy == 'collision':
        while os.path.exists(past_path):
            count += 1
            past_path = past_path[:cnt] + '.' + str(count)
    elif policy == 'replace':
        pass
    else:
        raise IOError('incorrect policy')
    return past_path


def make_dir(path):
    try:
        os.mkdir(path)
    except OSError:
        pass
