#!/usr/bin/env python2.7

"""
Smart rm installer.
"""

from setuptools import setup, find_packages

setup(
    name='smrm',
    version='0.0.1',
    packages=find_packages(exclude=['test']),
    entry_points={
        'console_scripts': {
            'smrm = smrm.args:main'
        }
    },
    description='Smart rm',
    author='Alex Nikiforov'
)
