def cached(function):
    memo = {}

    def wrapper(*args, **kwargs):
        cols = []
        vals = []
        for col, val in kwargs.items():
            cols.append(str(col))
            vals.append(str(val))
        cols = ", ".join(cols)
        vals = ", ".join(vals)
        alargs = str(cols) + str(vals) + str(args)
        if alargs in memo:
            return memo[alargs]
        else:
            tmp = function(*args, **kwargs)
            memo[alargs] = tmp
            return tmp
    return wrapper


@cached
def foo(i, **kwargs):
    print 'func'
    return 3 ** i

print foo(10, l=3, j=7)
print foo(10, l=6, j=7)
print foo(10, j=7, l=3)