import os
from unittest import TestCase
import shutil
from smrm.trash import Trash
import smrm.checker as checker
import smrm.config as config
import smrm.cleaner as cleaner


class TestTrash(TestCase):

    def setUp(self):
        if not os.path.exists(os.path.expanduser("~/test_trash")):
            os.makedirs(os.path.expanduser("~/test_trash"))
        self.path = os.path.expanduser("~/test_trash")
        self.t = Trash(dir_for_trash=os.path.expanduser("~/test_trash"))

    def test_make_dir(self):
        test_path = self.t.dir_for_trash
        checker.make_dir(test_path)
        self.assertTrue(os.path.exists(test_path))

    def test_delete_file_and_restore(self):
        file1 = './file.txt'
        with open(file1, 'w'):
            pass
        self.t.delete_file(file1)
        self.assertFalse(os.path.exists(file1))
        self.assertTrue(os.path.exists(os.path.join(self.t.file_path, 'file.txt')))
        self.assertTrue(os.path.exists(os.path.join(self.t.info_path, 'file.txt')))
        self.t.recovery(file1)
        self.assertFalse(os.path.exists(os.path.join(self.t.file_path, 'file.txt')))
        self.assertFalse(os.path.exists(os.path.join(self.t.info_path, 'file.txt')))
        self.assertTrue(os.path.exists(os.path.join('.', file1)))

    def test_delete_dir_and_restore(self):
        test_trash = Trash()
        file1 = 'file'
        checker.make_dir(file1)
        test_trash.delete_file(file1)
        self.assertFalse(os.path.exists(file1))
        self.assertTrue(os.path.exists(os.path.join(test_trash.file_path, 'file')))
        self.assertTrue(os.path.exists(os.path.join(test_trash.info_path, 'file')))
        test_trash.recovery(file1)
        self.assertFalse(os.path.exists(os.path.join(test_trash.file_path, 'file')))
        self.assertFalse(os.path.exists(os.path.join(test_trash.info_path, 'file')))
        self.assertTrue(os.path.exists(os.path.join('.', file1)))

    def test_auto_clean_date(self):
        date = config.parse_config(os.path.expanduser('~'), date=True)
        check = True
        cleaner.auto_delete_date(self.t)
        for i in os.listdir(self.t.info_path):
            with open(os.path.join(self.t.info_path, i), 'r') as f:
                date_file = f.read().split('\n')[1]
                if date_file < date:
                    self.fail('error')
        self.assertEqual(check, True)

    def test_auto_clean_size(self):
        file1 = './file1.txt'
        file2 = './file2.txt'
        with open(file1, 'w'):
            pass
        with open(file2, 'w'):
            pass
        count = len(os.listdir(self.t.file_path))
        count_del = config.parse_config(os.path.expanduser('~'), size=True)
        cleaner.auto_delete_size(self.t)
        cur_count = len(os.listdir(self.t.file_path))
        self.assertTrue(str(count_del) >= str(count-cur_count))

    def test_remove_by_re(self):
        file1 = '/home/alexander/isp.labs/lab1/lab/re/hello.txt'
        file2 = '/home/alexander/isp.labs/lab1/lab/re/hello2.txt'
        with open(file1, 'w'):
            pass
        with open(file2, 'w'):
            pass
        self.t.delete_by_re('/home/alexander/isp.labs/lab1/lab/re', 'hello')
        for i in [file1, file2]:
            self.assertFalse(os.path.exists(i))
            self.assertTrue(os.path.exists(os.path.join(self.t.file_path, os.path.split(i)[1])))
            self.assertTrue(os.path.exists(os.path.join(self.t.info_path, os.path.split(i)[1])))

    def test_clean_trash(self):
        self.t.delete_trash()
        self.assertEqual(0, len(os.listdir(self.t.file_path)))
        self.assertEqual(0, len(os.listdir(self.t.file_path)))

    def test_checker_in_trash(self):
        folder = self.t.file_path
        try:
            checker.in_trash(folder, self.t)
            self.fail('error')
        except OSError:
            self.assertTrue(True)
        folder = self.t.info_path
        try:
            checker.in_trash(folder, self.t)
            self.fail('error')
        except OSError:
            self.assertTrue(True)

    def test_checker_path_trash(self):
        my_path = self.t.trash_path
        try:
            checker.path_trash(my_path, self.t)
            self.fail('error')
        except OSError:
            self.assertTrue(True)

    def test_checker_path_exist_collision(self):
        self.t.delete_trash()
        with open('test123', 'w'):
            pass
        self.t.delete_file('test123')
        new_path = checker.path_exist_checker('test123', self.t)[0]
        self.assertEqual('test123.1', os.path.split(new_path)[1])

    def test_checker_path_exist_replace(self):
        self.t.politics_delete = 'replace'
        self.t.delete_trash()
        with open('test123', 'w'):
            pass
            self.t.delete_file('test123')
        new_path = checker.path_exist_checker('test123', self.t)[0]
        self.assertEqual('test123', os.path.split(new_path)[1])

    def test_remove_object(self):
        checker.make_dir('directory')
        checker.remove_object('directory')
        with open('test123', 'w'):
            pass
        checker.remove_object('test123')
        self.assertFalse(os.path.exists('directory'))
        self.assertFalse(os.path.exists('test123'))

    def test_exist_restore_collision(self):
        past_path = './test123'
        with open('test123', 'w'):
            pass
        past_path = checker.exist_restore_file(past_path, 'collision')
        self.assertEqual(os.path.split(past_path)[1], 'test123.1')

    def test_exist_restore_replace(self):
        past_path = './test123'
        with open('test123', 'w'):
            pass
        past_path = checker.exist_restore_file(past_path, 'replace')
        self.assertEqual(os.path.split(past_path)[1], 'test123')

    def tearDown(self):
        shutil.rmtree(self.path)
