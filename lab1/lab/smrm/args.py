import argparse
import os
import config as config
from trash import Trash
import checker


def create_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--silent', action='store_true', help='return only code')
    parser.add_argument('-d', '--dry_run', action='store_true', help='only write about changes, not perform him')
    parser.add_argument('-f', '--force', action='store_true', help='do not ask anything')
    parser.add_argument('-i', '--interactive', action='store_true', help='ask confirm each operations')

    subparsers = parser.add_subparsers(title='subcommands', description='valid subcommands', dest='com')

    subparsers.add_parser('list', help='show trash')
    parser_delete = subparsers.add_parser('delete', help='remove object on trash')
    parser_restore = subparsers.add_parser('restore', help='restore deleted objects')
    parser_del_re = subparsers.add_parser('delre', help='delete object by regex')
    subparsers.add_parser('clear', help='delete all objects in trash')
    parser_config = subparsers.add_parser('config', help='create configuration file, which determinate work trash')

    parser_config.add_argument('-pt', '--path', nargs='?', action='store')
    parser_config.add_argument('-sz', '--size', nargs='?', action='store')
    parser_config.add_argument('-dt', '--date', nargs='?', action='store')
    parser_config.add_argument('-pl', '--policy', nargs='?', action='store')
    parser_config.add_argument('-cl', '--cleaning', nargs='?', action='store')
    parser_config.add_argument('-st', '--size_trash', nargs='?', action='store')
    parser_config.add_argument('-sh', '--show', action='store_true')
    parser_restore.add_argument('name', nargs='+')
    parser_delete.add_argument('name', nargs='+')
    parser_del_re.add_argument('re', type=str)
    return parser.parse_args()


def main():
    args = vars(create_parser())
    path_config = os.path.expanduser('~')
    checker.check_config(path_config)
    dir_for_trash = config.parse_config(path_config, path=True)
    policy_clean = config.parse_config(path_config, cleaning=True)
    policy_delete = config.parse_config(path_config, policy=True)
    policy_clean_date = config.parse_config(path_config, date=True)
    policy_clean_size = config.parse_config(path_config, size=True)
    size_trash = config.parse_config(path_config, size_trash=True)
    trash = Trash(args['dry_run'], args['silent'], args['force'], args['interactive'], dir_for_trash,
                  policy_clean, policy_delete, policy_clean_date, policy_clean_size, size_trash)

    if args['com'] == 'config':
        config.create_config(path_config, path=args['path'],
                             date=args['date'],
                             size=args['size'],
                             policy=args['policy'],
                             cleaning=args['cleaning'],
                             size_trash=args['size_trash'])
        if args['show']:
            config.show_config(path_config)
    else:

        if args['com'] == 'list':
            trash.trash_list()

        if args['com'] == 'restore':
            for j in args['name']:
                trash.recovery(j)

        if args['com'] == 'delete':
            for i in args['name']:
                trash.delete_file(i)

        if args['com'] == 'delre':
            path = os.getcwd()
            trash.delete_by_re(path, args['re'])

        if args['com'] == 'clear':
            trash.delete_trash()

if '__main__' == __name__:
    main()
