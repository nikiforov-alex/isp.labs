import datetime
import os
import re
import shutil
import checker
import cleaner
import logging


class Trash(object):
    """
    based function on trash
    """
    def __init__(self, dry_run=False, silent=False, force=False,
                 interactive=False, dir_for_trash=os.path.expanduser('~'),
                 policy_clean='size', policy_delete='collision',
                 policy_clean_date=2, policy_clean_size=datetime.datetime.today(),
                 name='trash', size_trash=10000):
        self.dry_run = dry_run
        self.silent = silent
        self.force = force
        self.interactive = interactive
        self.dir_for_trash = dir_for_trash
	self.name = name
        self.trash_path = os.path.join(self.dir_for_trash, name)
        self.info_path = os.path.join(self.trash_path, 'info')
        self.file_path = os.path.join(self.trash_path, 'files')
        self.policy_clean = policy_clean
        self.policy_delete = policy_delete
        self.policy_clean_date = policy_clean_date
        self.policy_clean_size = policy_clean_size
        self.size_trash = size_trash
        self.make_trash()
        logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s',
                            level=logging.INFO, filename=u'/home/alexander/isp.labs/lab1/lab/mylog.log')

    def make_trash(self):
        checker.make_dir(self.trash_path)
        checker.make_dir(self.info_path)
        checker.make_dir(self.file_path)

    def delete_file(self, path):
        """
        delete objects
        """
        path = os.path.realpath(path)
        folder, name = os.path.split(path)
        checker.access(path, self)
        checker.exist_file(path, self)
        if not checker.interactive_mode("Delete %s :" % path, self):
            logging.info('Cancel deletion in interactive mode')
            exit()
        checker.in_trash(folder, self)
        checker.path_trash(path, self)
        new_path, new_path_info = checker.path_exist_checker(name, self)
        checker.check_size_object(checker.get_size_trash(path), self)
        while checker.get_size_trash(self.trash_path) + checker.get_size_trash(path) > self.size_trash:
            answer_clean = checker.force_ascker('The basket is full, begin auto-cleaning trash?', self)
            if cleaner.auto_cleaning(answer_clean, self):
                answer = checker.force_ascker("remove the object forever?(y/n)", self)
                checker.ascker_remove(answer, path, self)
            else:
                logging.info('Automatic cleaning was performed')
        if not self.dry_run:
            os.rename(path, new_path)
            with open(new_path_info, 'w') as f_1:
                f_1.writelines(os.path.realpath(path) + '\n')
                f_1.writelines(str(datetime.datetime.now()) + '\n')
        checker.silent_code('Object %s successfully deleted' % name, self)
        logging.info('Object %s successfully deleted' % name)
        

    def recovery(self, name_rec):
        """
        restore deleted object from trash
        """
        new_path = os.path.join(self.file_path, name_rec)
        path_info = os.path.join(self.info_path, name_rec)
        if not checker.interactive_mode("Restore %s :" % name_rec, self):
            logging.info('Cancel deletion in interactive mode')
            exit()
        with open(path_info, 'r') as f:
            past_path = f.readline()[:-1]
        past_path = checker.exist_restore_file(past_path, self.policy_delete)
        if not self.dry_run:
            os.remove(path_info)
            os.rename(new_path, past_path)
        checker.silent_code('Object %s successfully restored' % name_rec, self)
        logging.info('Object %s successfully restored' % name_rec)
       
    def delete_by_re(self, path, re_file):
        """
        deleted object by regular expression
        """
        objects = os.listdir(path)
        for obj in objects:
            path_object = os.path.join(path, obj)
            if re.match(re_file, obj):
                self.delete_file(path_object)
            else:
                if os.path.isdir(path_object):
                    self.delete_by_re(path_object, re_file)
        logging.info('Objects successfully deleted by regex')

    def trash_list(self):
        """
        showed current objects from trash
        """
        try:
            max_date, max_name, max_len_size = 10, 6, 6
            for i in sorted(os.listdir(self.file_path)):
                path_info = os.path.join(self.info_path, i)
                if max_name < len(i):
                    max_name = len(i)
                size = len(str(checker.get_size_trash(os.path.join(self.file_path, i))))
                if size > max_len_size:
                    max_len_size = size
                with open(path_info, 'r') as f_1:
                    old_path = f_1.readline().strip()
                    if max_date < len(old_path):
                        max_date = len(old_path)
            print 'name'.center(max_name, '_'), \
                '|', 'old path'.center(max_date, '_'), \
                '|', 'date'.center(20, '_'), \
                '|', 'size'.center(max_len_size, '_')
            count = 1
            for i in sorted(os.listdir(self.file_path)):
                path_info = os.path.join(self.info_path, i)
                with open(path_info, 'r') as f_1:
                    old_path = f_1.readline().strip()
                    date_del = f_1.readline().strip()[:19]
                    size_file = checker.get_size_trash(os.path.join(self.file_path, i))
                print i.ljust(max_name), \
                    '|', old_path.center(max_date), \
                    '|', date_del.rjust(20), \
                    '|', str(size_file).center(max_len_size)
                count += 1
                if count % 10 == 0:
                    if raw_input('continue?') not in ['y', 'Y']:
                        break
            logging.info('showed trash')
        except:
            logging.exception('error in show trash')

    def delete_trash(self):
        """
        clear trash
        """
        if not checker.interactive_mode('begin clearing trash?:', self):
            logging.info('Cancel deletion in interactive mode')
            exit()
        for i in os.listdir(self.file_path):
            checker.silent_code('Object %s deleted from trash' % i, self)
        if not self.dry_run:
            shutil.rmtree(self.info_path)
            checker.make_dir(self.info_path)
            shutil.rmtree(self.file_path)
            checker.make_dir(self.file_path)
        checker.silent_code('trash is all clean', self)
        logging.info('trash is all clean')

if __name__ == '__main__':
    trash = Trash()
