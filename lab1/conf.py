import os
import re
import datetime
import json

def create_config(path_trash, date, count_size, policy, cleaning):
    """
    Create configuration file
    """
    if not os.path.exists(path_trash):
        path_trash = '/home/alexander/isp.labs/lab1/lab/'
    if not re.match(date, '[0-9]{4,4}-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]+'):
        date = str(datetime.datetime.now())
    if not count_size.isdigit():
        count_size = 2
    if policy not in ['replace', 'collision']:
        policy = 'collision'
    if cleaning not in ['size', 'date']:
        cleaning = 'size'
    with open('/home/alexander/isp.labs/lab1/lab/config_file_json', 'w') as conf1:
        conf1.write(json.dumps({'path': str(path_trash),
                                'date': str(date),
                                'size': str(count_size),
                                'policy': str(policy),
                                'cleaning': str(cleaning)}))
    with open('/home/alexander/isp.labs/lab1/lab/config_file', 'w') as conf:
        conf.writelines('path from trash is ' + '\n\t' + str(path_trash) + '\n\n')
        conf.writelines('Date for cleaning trash: ' + '\n\t' + str(date) + '\n\n')
        conf.writelines('Amount of largest removable objects is ' + '\n\t' + str(count_size) + '\n\n')
        conf.writelines('Delete and restore policy:' + '\n\t' + str(policy) + '\n\n')
        conf.writelines('Policy cleaning trash:' + '\n\t' + str(cleaning) + '\n\n')


create_config(path_trash='a', date='a', count_size='a', policy='a', cleaning='a')

